///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package regexapp;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.regex.Pattern;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.CellStyle;
//import org.apache.poi.ss.usermodel.CellType;
//import org.apache.poi.ss.usermodel.CreationHelper;
//import org.apache.poi.ss.usermodel.Font;
//import org.apache.poi.ss.usermodel.IndexedColors;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import regexapp.dto.LineDTO;
//
///**
// *
// * @author sergegildasayepi
// */
//
//public class FileProcessing {
//
//    private static final String[] columns = {"Date", "NOM FOURNISSEUR", "NOCONTRIBUABLE", "n facture", "nature", "TOTAL TTC", "TOTAL TVA", "prorata", "tva"};
//    
//    public static void processFile(String filePath,String directory){
//        List<LineDTO> lines = processXlsFile(filePath);
//        createFinalFile(lines,directory);
//        
//    }
//
//    private static List<LineDTO> processXlsFile(String filename) {
//        List<LineDTO> allLines = new ArrayList();
//        try {
//            FileInputStream excelFile = new FileInputStream(new File(filename));
//            Workbook workbook = new HSSFWorkbook(excelFile);
//            Sheet datatypeSheet = workbook.getSheetAt(0);
//            Iterator<Row> iterator = datatypeSheet.iterator();
//            Pattern replace = Pattern.compile("\\W");
//            int i = 1;
//            while (iterator.hasNext()) {
//                Row currentRow = iterator.next();
//                if (i != 1) {
//                    LineDTO line = new LineDTO();
//                    line.setNature("achat de marchandises");
//                    line.setProrata(100);
//                    Cell factureCell = currentRow.getCell(1);
//                    if (factureCell.getStringCellValue().length() > 11) {
//                        line.setFacture(StringUtils.normalizeSpace(replace.matcher(factureCell.getStringCellValue().substring(0, (factureCell.getStringCellValue().length() - 8))).replaceAll(" ")));
//
//                    } else {
//                        line.setFacture(StringUtils.normalizeSpace(replace.matcher(factureCell.getStringCellValue()).replaceAll("")));
//                    }
//
//                    //System.out.println(line.getFacture());
//                    Cell dateCell = currentRow.getCell(2);
//                    Date date1 = new SimpleDateFormat("ddMMyy").parse(StringUtils.normalizeSpace(replace.matcher(dateCell.getStringCellValue()).replaceAll(" ")));
//                    line.setDate(new SimpleDateFormat("dd/MM/yyyy").format(date1));
//                    //System.out.println(line.getDate());
//
//                    Cell fourCell = currentRow.getCell(4);
//                    line.setFournisseur(StringUtils.normalizeSpace(replace.matcher(fourCell.getStringCellValue()).replaceAll(" ")));
//
//                    Cell contribuableCell = currentRow.getCell(6);
//                    line.setNumContribuable(StringUtils.normalizeSpace(replace.matcher(contribuableCell.getStringCellValue()).replaceAll("")));
//
//                    Cell totalTvaCell = currentRow.getCell(8);
//
//                    if (totalTvaCell.getCellTypeEnum() == CellType.STRING) {
//                        //System.out.println(totalTvaCell.getStringCellValue() + "--");
//                        line.setTotalTVA(Double.valueOf(StringUtils.normalizeSpace(replace.matcher(totalTvaCell.getStringCellValue()).replaceAll(""))));
//                    } else if (totalTvaCell.getCellTypeEnum() == CellType.NUMERIC) {
//                        //System.out.println(totalTvaCell.getNumericCellValue() + "");
//                        line.setTotalTVA(totalTvaCell.getNumericCellValue());
//                    }
//
//                    Cell totalTTCCell = currentRow.getCell(9);
//                    if (totalTTCCell.getCellTypeEnum() == CellType.STRING) {
//                        line.setTotalTTC(Double.valueOf(StringUtils.normalizeSpace(replace.matcher(totalTTCCell.getStringCellValue()).replaceAll(""))));
//                    } else if (totalTTCCell.getCellTypeEnum() == CellType.NUMERIC) {
//                        line.setTotalTTC(totalTTCCell.getNumericCellValue());
//                    }
//                    allLines.add(line);
//                    System.out.println("line : " + line.toString());
//                }
//                i++;
//                System.out.println("i : " + i);
//
//            }
//            //createFinalFile(allLines);
//            return allLines;
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            return allLines;
//        } catch (IOException e) {
//
//            e.printStackTrace();
//            return allLines;
//        } catch (ParseException ex) {
//
//            Logger.getLogger(RegexApp.class.getName()).log(Level.SEVERE, null, ex);
//            return allLines;
//        }
//
//    }
//
//    private static void createFinalFile(List<LineDTO> lines, String directory) {
//        // Create a Workbook
//        try {
//            System.out.println("regexapp.RegexApp.createFinalFile() START ");
//            Workbook workbook = new HSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
//
//            /* CreationHelper helps us create instances of various things like DataFormat, 
//           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
//            CreationHelper createHelper = workbook.getCreationHelper();
//
//            // Create a Sheet
//            Sheet sheet = workbook.createSheet("Fichier_Final");
//
//            // Create a Font for styling header cells
//            Font headerFont = workbook.createFont();
//            headerFont.setBold(true);
//            headerFont.setFontHeightInPoints((short) 14);
//            headerFont.setColor(IndexedColors.RED.getIndex());
//
//            // Create a CellStyle with the font
//            CellStyle headerCellStyle = workbook.createCellStyle();
//            headerCellStyle.setFont(headerFont);
//
//            // Create a Row
//            Row headerRow = sheet.createRow(0);
//
//            // Create cells
//            for (int i = 0; i < columns.length; i++) {
//                Cell cell = headerRow.createCell(i);
//                cell.setCellValue(columns[i]);
//                cell.setCellStyle(headerCellStyle);
//            }
//
//            // Create Cell Style for formatting Date
//            CellStyle dateCellStyle = workbook.createCellStyle();
//            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
//
//            // Create Other rows and cells with employees data
//            int rowNum = 1;
//            for (LineDTO dto : lines) {
//                Row row = sheet.createRow(rowNum++);
//                Cell dateTrans = row.createCell(0);
//                dateTrans.setCellValue(dto.getDate());
//
//                row.createCell(0).setCellValue(dto.getDate());
//
//                row.createCell(1).setCellValue(dto.getFournisseur());
//
//                row.createCell(2).setCellValue(dto.getNumContribuable());
//
//                row.createCell(3).setCellValue(dto.getFacture());
//
//                row.createCell(4).setCellValue(dto.getNature());
//
//                row.createCell(5).setCellValue(dto.getTotalTTC());
//
//                row.createCell(6).setCellValue(dto.getTotalTVA());
//                row.createCell(7).setCellValue(dto.getProrata());
//                row.createCell(8).setCellValue(dto.getTotalTVA());
//            }
//
//            // Resize all columns to fit the content size
//            for (int i = 0; i < columns.length; i++) {
//                sheet.autoSizeColumn(i);
//                System.out.println("row i : " + i);
//            }
//
//            // Write the output to a file
//            FileOutputStream fileOut = new FileOutputStream(directory + "result_" + System.currentTimeMillis() + ".xls");
//            workbook.write(fileOut);
//            fileOut.close();
//
//            workbook.close();
//            System.out.println("regexapp.RegexApp.createFinalFile() END ");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//
//    }
//
//}
