/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author sergegildasayepi
 */
public class CodeInGame {

    public static void main(String[] args) throws Exception {

        Scanner in = new Scanner(System.in);
        System.out.println("saisir startNodeId ");
        int startNodeId = in.nextInt();
        System.out.println("startNodeId : " + startNodeId);
        System.out.println("Saisir dimension fromIds: " + startNodeId);
        int n = in.nextInt();
        int[] fromIds = new int[n];
        for (int i = 0; i < n; i++) {
            fromIds[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(fromIds));
        System.out.println("Saisir dimension toIds: " + startNodeId);
        int[] toIds = new int[n];
        for (int i = 0; i < n; i++) {
            toIds[i] = in.nextInt();
        }
        System.out.println(Arrays.toString(toIds));
        int endPointId = FindNetworkEndpoint(startNodeId, fromIds, toIds);
        System.out.println(endPointId);
    }

   

    public static int FindNetworkEndpoint(int startNodeId, int[] fromIds, int[] toIds) {
        if ((toIds.length <= 0)) {
            return 0;
        }
        List<Integer> toIdsList = Arrays.stream(toIds).boxed().collect(Collectors.toList());
        List<Integer> fromIdsList = Arrays.stream(fromIds).boxed().collect(Collectors.toList());

        boolean endFound = false;      // mon booléen si j'ai une fin de noeud
        boolean lapEnded = false;      // mon booléen si ma recherche tourne en rond
        boolean startSearch = true;    // mon booléen pour indiquer que je suis au départ de ma recherche

        int node = startNodeId;
        int indexNode = Integer.MIN_VALUE;   //parce que l'index 0 existe
        int oldIndex = Integer.MIN_VALUE;    //parce que l'index 0 existe

        while (!endFound && !lapEnded) // tant que je n'ai pas trouvé le noeud de fin 
        {                               // ou que je n'ai pas fais de tour complet

            if (!startSearch && (node == startNodeId)) // si je ne suis pas au départ de ma recherche 
            {                                           // et que je retrouve le même noeud,
                lapEnded = true;                        // c'est que j'ai effectué un tour
                indexNode = oldIndex;
                break;
            }
            if (startSearch) // la recherche a commencé
            {
                startSearch = false;
            }

            if (fromIdsList.contains(node)) {
                oldIndex = indexNode;
                indexNode = fromIdsList.indexOf(node);
                node = toIdsList.get(indexNode);
            } else {
                endFound = true;
            }

        }
        return indexNode;
    }

}
