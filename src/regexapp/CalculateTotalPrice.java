/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.Arrays;

/**
 *
 * @author sergegildasayepi
 */
public class CalculateTotalPrice {
    
//    public static int calculateTotalPrice(int[] prices, int discount) {
//        Arrays.sort(prices);
//        int  maxPrice  = prices[prices.length-1];
//        //int discountPrice =maxPrice - ((int) Math.ceil((maxPrice*discount)/100)) ;
//        double discountPrice =maxPrice*(1-discount/100) ;
//        int totalPrice = 0;
//        for(int i =0; i< prices.length-1; i++){
//            totalPrice+=prices[i];
//        }
//        // Write your code here
//        // To debug: System.err.println("Debug messages...");
//        
//        return  (int) Math.ceil(totalPrice+discountPrice);
//    } 
     
    public static int calculateTotalPrice(int[] prices, int discount) {
        Arrays.sort(prices);
        int  maxPrice  = prices[prices.length-1];
        
        int discountPrice = ((int) Math.floor(maxPrice -(maxPrice*discount)/100)) ;
        int totalPrice = 0;
        for(int i =0; i< prices.length-1; i++){
            totalPrice+=prices[i];
        }
        
        return totalPrice+discountPrice;
    } 
    
    
}
