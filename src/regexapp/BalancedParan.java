/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

/**
 *
 * @author sergegildasayepi
 */
public class BalancedParan {

    public static boolean check(String expression) {
        Map<Character, Character> openClosePair = new HashMap<>();
        openClosePair.put(')', '(');
        openClosePair.put(']', '[');

        Stack<Character> stack = new Stack<>();
        for (char ch : expression.toCharArray()) {
            if (openClosePair.containsKey(ch)) {
                if (stack.isEmpty() || !Objects.equals(stack.pop(), openClosePair.get(ch))) {
                    return false;
                }
            } else if (openClosePair.values().contains(ch)) {
                stack.push(ch);
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) throws Exception {

        System.out.println(check(""));
        System.out.println(check("[()]"));   // true
        System.out.println(check("(()[])")); // true
        System.out.println(check("([)]"));   // false
        System.out.println(check("(("));     // false
        System.out.println(check("[(()])")); // false

        System.out.println(check("([(([[(([]))]]))])"));   // true
        System.out.println(check("[](()()[[]])()[]([])")); // true
        System.out.println(check("([((([(([]))])))))])")); // false
        System.out.println(check("[](()()[[]])[][[([])")); // false

    }

}
