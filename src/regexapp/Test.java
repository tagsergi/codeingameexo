/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.math.BigInteger;
import java.util.stream.LongStream;


/**
 *
 * @author sergegildasayepi
 */
public class Test {

    public static void main(String[] args)  {
        System.out.println(1<<2);

        //System.out.println("combination : " + combination(2,4));
        LongStream.rangeClosed(1, 5).reduce(1, Long::sum);
    }
    
    static  long factorial(int n) {
        return LongStream.rangeClosed(1, n).reduce(1, (long x, long y) -> x *  y);
    }

    static  BigInteger factorialHavingLargeResult(int n) {
        BigInteger result = BigInteger.ONE;
        for (int i = 2; i <= n; i++)
            result = result.multiply(BigInteger.valueOf(i));
        return result;
    }


    static BigInteger combination(int k, int n){
        BigInteger multiply = factorialHavingLargeResult(k).multiply(factorialHavingLargeResult((n-k)));
        return factorialHavingLargeResult(n).divide(multiply);
    }

}
