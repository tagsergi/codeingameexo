/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.io.ObjectOutputStream;

/**
 *
 * @author sergegildasayepi
 */
public class TestA implements ITestA {

    public static void main(String[] args) throws Exception {
        
        int a = 1*Integer.MAX_VALUE;
        System.out.println(a);
        
    }

    static String sum(String... numbers) {
        double total = 0;
        for (String number : numbers) {
            System.out.println("number : " + number);
            total += Double.parseDouble(number);
            System.out.println("total : " + total);
        }
        return String.valueOf(total);
    }

    static int sumRange(int[] ints) {
        int sum = 0;
        for (int i = 1; i < ints.length; i++) {
            int n = ints[i];
            if (n >= 10 & n <= 100) {
                System.out.println(" n : " + n);
                sum += n;
                System.out.println("sum : " + sum);
            }
        }
        return sum;
    }

}
