/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;


/**
 *
 * @author sergegildasayepi
 */
public class Node {

    Node left, right;
    int value;

    public Node find(int v)  {
        Node current = this;
        while (current != null) {
            if (current.value == v) {
                return current;
            }
            // This will drop out of the loop naturally if there's no appropriate subnode
            current = v < current.value ? current.left : current.right;
        }
        return null;
    }

    public Node(Node left, Node right, int value) {
        this.left = left;
        this.right = right;
        this.value = value;
    }

    public Node(Node left) {
        this.left = left;
    }
    

    public Node(int value) {
        this.value = value;
    }
    
    
    
    
    public static void main(String[] args) throws Exception {
        Node root = new Node(9);
        
        root.left = new Node(7);
        root.right = new Node(13);
        
    }

}
