/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.EmptyStackException;

/**
 *
 * @author sergegildasayepi
 */
public class Stack {

    private Object[] elements;
    private int size = 0;

    public Stack(int initialCapacity) {
        elements = new Object[initialCapacity];
        ensureCapacity();
    }

    public void push(Object object) {
        elements[size++] = object;
    }

    public Object pop() {
        if (size == 0) {
            throw new EmptyStackException();
        }

        return elements[--size];
    }

    private void ensureCapacity() {
        if (elements.length == size) {
            Object[] old = elements;
            elements = new Object[2 * size + 1];
            System.arraycopy(old, 0, elements, 0, size);
        }
    }

    public static void main(String[] args) throws Exception {
        Stack stack = new Stack(10000);
        System.out.println(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()); // before using the stack

        for (int i = 0; i < 10000; i++) { // fill the stack
            stack.push("a large, large, large, large, string " + i);
        }
        for (int i = 0; i < 10000; i++) { // empty the stack
            System.out.println(stack.pop());
        }

        System.out.println(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()); // after using the stack
    }

}
