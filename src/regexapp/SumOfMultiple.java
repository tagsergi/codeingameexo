/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public class SumOfMultiple {
    
    public static Long solution(int n){
        long c = 0;
        for(int i=0;i<n;i++){
            if((i%3==0) || (i%5==0) )
                c+=i;
            if(i%15==0){
                c-=i;
            }
        }
        return c;
    }
    
    public static int gaussSummation(int n, int k){
        
       if(k>n)
           return 0;
        return (k/2)*(n/k)*(1+n/k);
    }
    
    public static void main(String[] args) {
        System.out.println("Solution : "+solution(1000));
        System.out.println("Solution : "+(gaussSummation(1000,3)+gaussSummation(1000,5)-gaussSummation(1000,15)));
        
    }
    
}
