/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import java.util.Arrays;

/**
 *
 * @author sergegildasayepi
 */
public class Temperature {
    
    public static void main(String[] args) {
        int[] str = {};
//        Arrays.stream(str).filter(i -> i != 0)
//                .reduce((a, b) -> abs(a) < abs(b) ? a : (abs(a) == abs(b) ? max(a, b) : b))
//                .ifPresent(System.out::println);
        System.out.println(computeClosestToZero(str));
    }
    
    public static int computeClosestToZero(int[] ts){
        if(ts.length==0){
            return 0;
        }
        return Arrays.stream(ts).filter(i -> i != 0)
                .reduce((a, b) -> abs(a) < abs(b) ? a : (abs(a) == abs(b) ? max(a, b) : b)).getAsInt();
    }
    
}
