/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public class CodeInGameStep {

    public static void main(String[] args) {
        
        
        System.out.println("#####################   calculate Version #####################");
        System.out.println(calculate(0));
        System.out.println(calculate(1));
        System.out.println(calculate(2));
        System.out.println(calculate(3));
        System.out.println(calculate(4));
        System.out.println(calculate(5));
        System.out.println(calculate(6));
        System.out.println(calculate(7));
        System.out.println(calculate(8));
        System.out.println(calculate(9));
        System.out.println(calculate(10));
        
        System.out.println("#####################   Modulo Version #####################");
        
        
        System.out.println(moduloVersion(0));
        System.out.println(moduloVersion(1));
        System.out.println(moduloVersion(2));
        System.out.println(moduloVersion(3));
        System.out.println(moduloVersion(4));
        System.out.println(moduloVersion(5));
        System.out.println(moduloVersion(6));
        System.out.println(moduloVersion(7));
        System.out.println(moduloVersion(8));
        System.out.println(moduloVersion(9));
        System.out.println(moduloVersion(10));

    }

    public static int calculate(int iterationNumber) {
        if (iterationNumber == 0) {
            return 0;
        }
        if (iterationNumber == 1) {
            return 1;
        }
        int position = 0;
        int step1 = 1;
        position = position + step1;
        iterationNumber = iterationNumber - 1;
        int step2 = -2;
        position = position + step2;
        iterationNumber -= 1;
        while (iterationNumber > 0) {
            position = position + (step2 - step1);
            int buffer = step1;
            step1 = step2;
            step2 = step2 - buffer;
            iterationNumber -= 1;
        }
        return position;
    }

    public static int moduloVersion(int number) {
        int rep = 0;
        switch (number % 6) {
            case 0:
                rep = 0;
                break;
            case 1:
                rep = 1;
                break;
            case 2:
                rep = -1;
                break;
            case 3:
                rep = -4;
                break;
            case 4:
                rep = -5;
                break;
            case 5:
                rep = -3;
                break;
        }
        return rep;
    }
}
