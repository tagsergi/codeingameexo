/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.Arrays;

/**
 *
 * @author sergegildasayepi
 */
public class BinarySearch {

    private static boolean check(int[] arr, int toCheckValue) {
        // check if the specified element 
        // is present in the array or not 
        // using Binary Search method 
        return (Arrays.binarySearch(arr, toCheckValue)>-1);
    }

    public static void main(String[] args) {

        // Get the array 
        int arr[] = {5, 1, 1, 9, 7, 2, 6, 10};

        // Get the value to be checked 
        int toCheckValue = 7;

        // Print the array 
        System.out.println("Array: "
                + Arrays.toString(arr));

        // Check if this value is 
        // present in the array or not 
        check(arr, toCheckValue);
    }

    static boolean exists(int[] ints, int k) {

        int debut = 0;
        int fin = ints.length - 1;
        while (debut <= fin) {
            int milieu = (debut + fin) / 2;
            if (ints[milieu] < k) {
                debut = milieu + 1;
            } else if (ints[milieu] > k) {
                fin = milieu - 1;
            } else {
                return true; // trouvé
            }
        }
        return false; // pas trouvé

    }

    static boolean exist(int[] ints, int k) {
        //for (int i : ints)
        //    if (i == k) return true;
        //return false;
        return (Arrays.binarySearch(ints, k) > -1);
    }

}
