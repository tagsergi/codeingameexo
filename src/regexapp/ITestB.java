/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public interface ITestB extends ITest{
    default String processB() {
        return getInstance().processB();
    }

    default ITestB getInstance() {
        return new ITestB(){
            public String processB() {
                return "Process B";
            }
        };
    }
    
}
