/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public class Reshape {
    
    public static String reshape(int n , String words){
        String working = words.replace(" ", "");
        String response = "";
        for(int i =0; i<working.length(); i=i+n){
            if(i+n <= working.length() )
                response = response + working.substring(i, i+n)+" ";
            else
                response = response + working.substring(i, working.length())+" ";
        }
        return response;
        
    }
    
    public static void main(String[] args) {
        String s= "1 23 456";
        System.out.println(reshape(2, s));
    }
}
