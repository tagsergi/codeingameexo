/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sergegildasayepi
 */
public class MonteCarlo {

    public static void main(String[] args) {
        MonteCarlo m = new MonteCarlo();
        m.start();
    }

    public void start() {
        List<Coordonne> pts = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            double x = Math.random();
            double y = Math.random();
            pts.add(new Coordonne(x, y));
        }
        System.out.println("regexapp.MonteCarlo.start() : "+approx(pts));
    }

    public float approx(List<Coordonne> pts) {

        int inside = 0;
        for (int i = 0; i < pts.size(); i++) {

            double x = pts.get(i).getX() * 2 - 1;
            double y = pts.get(i).getY() * 2 - 1;

            if ((x * x + y * y) < 1) {
                inside++;
            }
        }
        return (float) (4.0 * inside / pts.size());

    }

    private class Coordonne {

        private double x;

        private double y;

        public Coordonne(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return x;
        }

        public void setX(double x) {
            this.x = x;
        }

        public double getY() {
            return y;
        }

        public void setY(double y) {
            this.y = y;
        }

    }

}
