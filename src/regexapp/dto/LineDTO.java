/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp.dto;

/**
 *
 * @author sergegildasayepi
 */
public class LineDTO {
    
    private String date;
    
    private String fournisseur;
    
    private String numContribuable;
    
    private String facture;
    
    private String nature;
    
    private Double totalTTC;
    
    private Double totalTVA;
    
    private Integer prorata;
    
    private Double tva;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFournisseur() {
        return fournisseur;
    }

    public void setFournisseur(String fournisseur) {
        this.fournisseur = fournisseur;
    }

    public String getNumContribuable() {
        return numContribuable;
    }

    public void setNumContribuable(String numContribuable) {
        this.numContribuable = numContribuable;
    }

    public String getFacture() {
        return facture;
    }

    public void setFacture(String facture) {
        this.facture = facture;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Double getTotalTTC() {
        return totalTTC;
    }

    public void setTotalTTC(Double totalTTC) {
        this.totalTTC = totalTTC;
    }

    public Double getTotalTVA() {
        return totalTVA;
    }

    public void setTotalTVA(Double totalTVA) {
        this.totalTVA = totalTVA;
    }

    public Integer getProrata() {
        return prorata;
    }

    public void setProrata(Integer prorata) {
        this.prorata = prorata;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    @Override
    public String toString() {
        return "LineDTO{" + "date=" + date + ", fournisseur=" + fournisseur + ", numContribuable=" + numContribuable + ", facture=" + facture + ", nature=" + nature + ", totalTTC=" + totalTTC + ", totalTVA=" + totalTVA + ", prorata=" + prorata + ", tva=" + tva + '}';
    }
    
    
    
}
