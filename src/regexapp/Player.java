package regexapp;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Player {

    public static String solve(int clawPos, int[] boxes, boolean boxInClaw) {
        // Write your code here
        // To debug: System.err.println("Debug messages...");

        int sum = 0;
        int moyenne = 0;
        System.err.println(Arrays.toString(boxes));
        int maxIndex = getGreaterElementIndex(boxes);
        int minIndex = getLowerElementIndex(boxes);

        // verifie si le bras est libre ou non
        if(boxInClaw){
            for(int i : boxes){
                sum+=i;
            }
            moyenne= (sum+1)/boxes.length;
            System.err.println("moyenne : "+moyenne);
            System.err.println("boxes[clawPos] : "+boxes[clawPos]);
            if(moyenne<1){
                if (minIndex < clawPos) {
                    return "LEFT";
                } else if(minIndex > clawPos){
                    return "RIGHT";
                }else{
                    return "PLACE";
                }
            }else{
                if((boxes[clawPos]>=moyenne) ){

                    if(minIndex<clawPos){
                        return "LEFT";
                    }else {
                        return "RIGHT";
                    }
                }else {
                    System.err.println(" NOus faisons un PLACE");
                    return "PLACE";
                }
            }


        }else { // bras vide
            for (int i : boxes) {
                sum += i;
            }
            moyenne = sum / boxes.length;
            if (moyenne < 1) {
                if (maxIndex < clawPos) {
                    return "LEFT";
                } else if (maxIndex > clawPos) {

                    return "RIGHT";
                } else {
                    return "PICK";
                }
            } else {
                if (boxes[clawPos] > moyenne) { //nombre de colis supérieur à la moyenne
                    return "PICK";
                } else {
                    if (maxIndex < clawPos) {
                        return "LEFT";
                    } else {
                        return "RIGHT";
                    }
                }
            }


        }
    }

    public static void main(String[] args) {
        int[] tab = {4, 9, 5, 3, 11, 3, 11};
        System.out.println(getGreaterElementIndex(tab));
        System.out.println(getLowerElementIndex(tab));
    }

    static int getGreaterElementIndex(int[] boxes) {
        return IntStream.range(0, boxes.length).parallel().
                reduce((a, b) -> boxes[a] < boxes[b] ? b : a).orElse(-1);
    }

    static int getLowerElementIndex(int[] boxes) {
        return IntStream.range(0, boxes.length).parallel().
                reduce((a, b) -> boxes[a] > boxes[b] ? b : a).orElse(-1);
    }

    static int checkIfAllElementsAreEqual(int[] boxes) {
        return IntStream.range(0, boxes.length).parallel().
                reduce((a, b) -> boxes[a] - boxes[b]).orElse(-1);
    }


}
