/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public class TestRandom {

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            System.out.println(getRandomNumberInRange(1, 4));
        }

    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int) (Math.random() * ((max - min) + 1)) + min;
    }

}
