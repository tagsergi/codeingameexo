/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.Arrays;

/**
 *
 * @author sergegildasayepi
 */
public class MonteCarloJ {

    public static void main(String[] args) {
        
        double[][] rands = new double[100000][2];
        for(int i=0; i<rands.length;i++){
            rands[i][0] = Math.random();
            rands[i][1] = Math.random();
        }
        System.out.println(Arrays.deepToString(rands));
        System.out.println(getPi(rands));
        //System.out.println(getPi(1000000));
//        System.out.println(getPi(1000000));
//        System.out.println(getPi(10000000));
//        System.out.println(getPi(100000000));

    }

    public static double getPi(int numThrows) {
        int inCircle = 0;
        for (int i = 0; i < numThrows; i++) {
            //a square with a side of length 2 centered at 0 has 
            //x and y range of -1 to 1
            double randX = (Math.random() * 2) - 1;//range -1 to 1
            double randY = (Math.random() * 2) - 1;//range -1 to 1
            //distance from (0,0) = sqrt((x-0)^2+(y-0)^2)
            double dist = Math.sqrt(randX * randX + randY * randY);
            //^ or in Java 1.5+: double dist= Math.hypot(randX, randY);
            if (dist < 1) {//circle with diameter of 2 has radius of 1
                inCircle++;
            }
        }
        return 4.0 * inCircle / numThrows;
    }
    
    public static double getPi(double [][] tab) {
        int inCircle = 0;
        for (double[] doubles : tab) {
            //a square with a side of length 2 centered at 0 has
            //x and y range of -1 to 1
            double randX = (doubles[0] * 2) - 1;//range -1 to 1
            double randY = (doubles[1] * 2) - 1;//range -1 to 1
            double dist = randX * randX + randY * randY;
            if (dist < 1) {//circle with diameter of 2 has radius of 1
                inCircle++;
            }
        }
        return 4.0 * inCircle / tab.length;
    }

}
