/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public class Monnaie {

    long coin2 = 0;
    long bill5 = 0;
    long bill10 = 0;

}

class Solution {

    static Monnaie optimalChange(long s) {
        Monnaie c = new Monnaie();
        if (s >= 10) {
            c.bill10 = s / 10;
            if (s % 10 >= 5) {
                c.bill5 = (s % 10) / 5;
                if ((s % 10) % 5 >= 2) {
                    c.coin2 = ((s % 10) % 5) / 2;
                }
            } else if (s % 10 >= 2) {
                c.coin2 = (s % 10) / 2;
            }
        } else if (s >= 5 && s < 10) {
            c.bill5 = s / 5;
            if (s % 5 >= 2) {
                c.coin2 = (s % 5) / 2;
            }
        } else if (s > 2 && s < 5) {
            c.coin2 = s / 2;
        }
        return c;
    }
}
