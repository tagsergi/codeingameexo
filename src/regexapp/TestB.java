/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

/**
 *
 * @author sergegildasayepi
 */
public class TestB {
    
    public static void main(String[] args) {
        Animal a = new Cat("chi");
        Animal b =  new Dog("boubou");

        if(a instanceof Cat){
            System.out.println( a.getName());
        }
        if(b instanceof Dog){
            System.out.println( b.getName());
        }
    }
    
}
