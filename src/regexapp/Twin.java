/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.util.Arrays;

/**
 *
 * @author sergegildasayepi
 */
public class Twin {
    
    
    public static void main(String[] args) {
        System.out.println(isTwin("hello", "world"));// false
        System.out.println(isTwin("abc", "cab"));// true
        System.out.println(isTwin("Lookout", "outlook"));// true    
    }

    public static boolean isTwin(String a, String b) {
        byte[] ab = a.toLowerCase().getBytes();
        byte[] bb = b.toLowerCase().getBytes();
        Arrays.sort(ab);
        Arrays.sort(bb);
        return new String(ab).equals(new String(bb));
    }
    
}
