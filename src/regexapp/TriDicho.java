/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regexapp;

import java.io.IOException;

/**
 *
 * @author sergegildasayepi
 */
public class TriDicho {

    public static void main(String args[]) throws IOException // Point d'entree du programme
    {
        int tnb[] = {101254, 54, 2, 321326, 1255, 10128, 65, 4, 7, 4, 10, 5, 3, 7, 8};
        int[] ttrie = new int[tnb.length];
        affichetableau(tnb);
        trier(tnb, ttrie);
        affichetableau(ttrie);
        //System.out.println(round((double)1.5));
    }

    public static void affichetableau(int tab[]) throws IOException {
        for (int i = 0; i < tab.length - 1; i++) {
            System.out.print(tab[i] + " - ");
        }
        System.out.println(tab[tab.length - 1]);
    }

    public static void trier(int tab1[], int tab2[]) throws IOException {
        int debut = 0;
        int fin = 0;
        int pos = 0;

        for (int i = 0; i < tab2.length; i++) {
            pos = rechercheposition(tab1[i], tab2, debut, fin);
            fin = i;
            insertion(tab1[i], tab2, pos);
        }
    }

    public static void insertion(int val, int tab[], int pos) throws IOException {
        int zr;

        for (int i = tab.length - 2; i > pos - 1; i--) {
            zr = tab[i];
            if (pos != i) {
                tab[i] = tab[i - 1];
            } else {
                tab[i] = val;
            }
            tab[i + 1] = zr;
        }
    }

    public static int round(double nb) throws IOException {
        int monint = (int) nb;
        double mondouble = (double) monint;
        if (mondouble != nb) {
            return (int) nb + 1;
        }
        return (int) nb;
    }

    public static int rechercheposition(int val, int tab[], int debut, int fin) throws IOException {
        int milieu;
        do {
            milieu = round((double) ((double) debut + (double) fin) / 2);
            //System.out.println("valeur ="+val+" debut ="+debut+" fin ="+fin+" milieu ="+milieu);
            //affichetableau(tab);
            if (tab[milieu] == 0 & fin == 0) {
                return 0;
            }                // si la valeur du tableau est 0 et que la fin est 0 retourne 0, dans le cas du premier nombre inscrit au tableau
            if (tab[milieu] <= val & debut == fin) {
                return fin + 1;
            }    // si la valeur est superieure et qu'il n'y a plus d'ecart entre fin et debut retourne fin+1
            if (tab[milieu] > val & debut == fin) {
                return fin;
            }        // si la valeur est inferieure et qu'il n'y a plus d'ecart entre fin et debut retourne fin+1
            if (tab[milieu] > val) // quand la valeur du tableau coordonee milieu est superieure a la valeur a entrer
            {
                if (fin - debut == 1) {
                    fin = fin - 1;
                } // si il ne reste que deux cases, alors la deuxieme est inutile
                else {
                    fin = milieu;
                };                    // sinon fin est initialise avec milieu
            }
            if (tab[milieu] <= val) {
                debut = milieu;
            }
            // si la valeur du tableau a la coordonee de milieu est inferieure ou egale a la valeur a entrer, alors on oublie tout ce qui se trouve avant
        } 
        while (milieu != -1);                                            // condition improbable pour rester dans la boucle
        return 0;                                                        // retour obligatoire pour le compilateur mais qui n'a jamais lieu
    }
}
// *****************************************************************
// Fin de la classe TriDicho
// *****************************************************************
